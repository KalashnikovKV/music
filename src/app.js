import Header from "./Header";
import Main from "./mainDefer";
import Footer from "./Footer";

const body = document.getElementById("body");

const render = () => {
  body.innerHTML = `
  ${Header()}
  ${Main()}
  ${Footer()}
  `;
};

export default render;
