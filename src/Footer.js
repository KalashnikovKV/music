const footer = () => `
<footer>
<div class="container-footer">
  <nav class="nav-footer">
    <ul>
      <li class="li-footer">About As</li>
      <li class="li-footer">Contact</li>
      <li class="li-footer">CR Info</li>
      <li class="li-footer">Twitter</li>
      <li class="li-footer">Facebook</li>
      <!-- <li class="li-header"><img src="../../../assets/icons/user.svg" alt="user.svg"></li> -->
    </ul>
  </nav>
  <!-- <button class="mobile-header-menu"><img src="../../../assets/icons/menu.svg" alt="menu"></button> -->      
</div>
</footer>
`;

export default footer;
