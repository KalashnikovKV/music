const header = () => `
<header>
<div class="container-header">
  <a class="logo" href="#"><img src="./assets/icons/logo.svg" alt="logo">Simo</a>
  <nav class="nav-header">
    <ul>
      <li class="li-header" id="Discover">Discover</li>
      <li class="li-header" id="Join">Join</li>
      <li class="li-header" id="Sign In">Sign In</li>
      <!-- <li class="li-header"><img src="../../../assets/icons/user.svg" alt="user.svg"></li> -->
    </ul>
  </nav>
  <!-- <button class="mobile-header-menu"><img src="../../../assets/icons/menu.svg" alt="menu"></button> -->      
</div>
</header>
`;

export default header;
