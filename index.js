import render from "./src/app";
render();

const main = document.getElementById("main");
const imageMain = document.getElementById("image-main");

let mainClassList = main.classList;

const land = document.createElement("land");
land.innerHTML = `
<div class="land-form" id="land-form">
  <h1>Feel the music</h1>
  <span>Stream over 10 million songs with one click</span>
  <button id="land-form-join-btn">Join now</button>
</div>
`;
main.append(land);

document
  .getElementById("land-form-join-btn")
  .addEventListener("click", function () {
    main.innerHTML = `
    <div class="logIn-form" id="logIn-form">
    <div class="logIn-container">
    <div class="logIn-data">
    <span>Name:</span>
    <input type="text">
    </div>
    <div class="logIn-data">
    <span>Password:</span>
    <input type="text">
    </div>
    <div class="logIn-data">
    <span>e-mail:</span>
    <input type="text">
    </div>
    </div>
    <button>Join now</button>
    </div>
    `;
    imageMain.innerHTML = `<img src="./assets/images/background-page-sign-up.png" alt="background-page-sign-up.png">`;
    imageMain.style.marginLeft = "353px";
    main.classList.add("signUp");
    main.classList.remove("lang");
    main.classList.remove("LogIn");
    main.classList.remove("feature");
    document.getElementById("logIn-form").style.display = "flex";
  });

document.getElementById("Discover").addEventListener("click", function () {
  if (mainClassList.value !== "signUp") {
    main.innerHTML = `
    <div class="feature-form" id="feature-form">
    <h1>Discover new music</h1>
    <div class="feature-buttons">
      <button>Join now</button>
      <button class="middle-btn">Join now</button>
      <button>Join now</button>
    </div>
    <span>By joing you can benefit by listening to the latest albums released</span>
  </div>
    `;
    imageMain.innerHTML = `<img src="./assets/images/music-titles.png" alt="music-titles.png">`;
    imageMain.style.marginLeft = "1246px";
    imageMain.style.marginTop = "284px";
    imageMain.style.minWidth = "512px";
    main.classList.add("feature");
    main.classList.remove("signUp");
    main.classList.remove("logIn");
    main.classList.remove("lang");
    document.getElementById("feature-form").style.display = "flex";
  } else {
    imageMain.innerHTML = `<img src="./assets/images/background-page-landing.png" alt="background-page-landing">`;
    imageMain.style.marginLeft = "172px";
    imageMain.style.marginTop = "0px";
    imageMain.style.minWidth = "1920px";
    main.classList.add("lang");
    main.classList.remove("signUp");
    main.classList.remove("logIn");
    main.classList.remove("feature");
    main.append(land);
    document.getElementById("logIn-form").style.display = "none";
  }
});

document.getElementById("Join").addEventListener("click", function () {
  main.innerHTML = `
    <div class="logIn-form" id="logIn-form">
    <div class="logIn-container">
    <div class="logIn-data">
    <span>Name:</span>
    <input type="text">
    </div>
    <div class="logIn-data">
    <span>Password:</span>
    <input type="text">
    </div>
    <div class="logIn-data">
    <span>e-mail:</span>
    <input type="text">
    </div>
    </div>
    <button>Join now</button>
    </div>
    `;
  imageMain.innerHTML = `<img src="./assets/images/background-page-sign-up.png" alt="background-page-sign-up.png">`;
  imageMain.style.marginLeft = "353px";
  imageMain.style.marginTop = "0px";
  imageMain.style.minWidth = "1920px";

  main.classList.add("signUp");
  main.classList.remove("lang");
  main.classList.remove("logIn");
  main.classList.remove("feature");
  document.getElementById("logIn-form").style.display = "flex";
});
